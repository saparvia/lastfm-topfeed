#!/usr/bin/env python
# Tool to produce RSS feed from Last.fm top {album,artist,track} feeds
import sys
import uuid
import time
import datetime
import urllib2
import xml.etree.ElementTree as etree
import cPickle as pickle
import re

try:
	from config import API_KEY
except ImportError:
	print "Could not load API key. Create a file named 'config.py' containing your Last.fm API key in a variable called 'API_KEY'"
	sys.exit(1)

# TODO: Leap years
# Times two to get cyclic calculation correct
days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]*2

feed_template = u'''
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
<title>Top {list_type} for {user}</title>
<id>urn:uuid:{id}</id>
<updated>{updated}</updated>
<author><name>lastfm_topfeed</name></author>
{entries}
</feed>
'''

entry_template = u'''
<entry>
<title>Top {list_type} {period}</title>
<id>urn:uuid:{id}</id>
<updated>{updated}</updated>
<content type='html'>{items}</content>
</entry>
'''

item_template = u"&lt;a href='{url}'&gt;{name}&lt;/a&gt;"

valid_types = ('artists', 'albums', 'tracks')
valid_periods = ('overall', '7day', '3month', '6month', '12month')
if len(sys.argv) < 6:
	print 'Usage: %s username list_type period number_of_items number_of_entries'%sys.argv[0]
	print 'list_type is one of '+', '.join(valid_types)
	print 'period is one of '+', '.join(valid_periods)
	print 'numer_of_items tells how many top-items will be included in each entry'
	print 'numer_of_entries tells how many entries will be included in the feed'
	sys.exit(1)

user = sys.argv[1]
list_type = sys.argv[2]
period = sys.argv[3]
n_items = int(sys.argv[4])
n_entries = int(sys.argv[5])

if list_type not in valid_types:
	print 'WARNING: Unknown list type. Should probably be one of %s'%', '.join(valid_types)

if period not in valid_periods:
	print 'WARNING: Unknown period. Should probably be one of %s'%', '.join(valid_types)

# Calculate start date
today = datetime.date.today()
m = re.match(r'(\d+)(.*)', period)
if not m:
	period_str = 'overall'
else:
	if m.group(2) == 'day':
		period_days = int(m.group(1))

	# Not correct, but close enough
	if m.group(2) == 'month':
		period_days = sum(days[today.month*2-int(m.group(1)):today.month*2])
	delta = datetime.timedelta(period_days)
	start = today-delta
	if start.year != today.year:
		period_str = 'from %s to %s'%(start.strftime('%Y-%m-%d'),today.strftime('%Y-%m-%d'))
	else:
		period_str = 'from %s to %s'%(start.strftime('%m-%d'),today.strftime('%m-%d'))

cache_path = 'cache_%s_%s_%s.dat'%(user,list_type, period)

xml = urllib2.urlopen('http://ws.audioscrobbler.com/2.0/?method=user.gettop%s&user=%s&api_key=%s&period=%s&limit=%s'%(list_type, user, API_KEY, period, n_items)).read()
doc = etree.fromstring(xml).find('top%s'%list_type)

updated = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())

items = [item_template.format(name=x.find('name').text,url=x.find('url').text) for x in doc.findall(list_type[:-1])[:n_items]] # Drop of last 's' from type
if len(items) == 1:
	items_str = items[0]
else:
	items_str = ', '.join(items[:-1]) + ' and ' + items[-1]
x = entry_template.format(list_type=list_type, period=period_str, items=items_str , id=uuid.uuid1(), updated=updated)

try:
	with open(cache_path, 'r') as cachefile:
		entries = pickle.load(cachefile)
except:
	entries = []

entries.append(x)

if len(entries) > n_entries:
	entries.pop(0)	

with open(cache_path, 'w') as cachefile:
	pickle.dump(entries, cachefile)

entries.reverse()

entries = '\n'.join(entries)

print feed_template.format(list_type=list_type, user=user, id=uuid.uuid3(uuid.UUID('d4268dda-f736-11e0-a439-3408049da7e4'), 'lastfm_topfeed'), updated=updated, entries=entries)
